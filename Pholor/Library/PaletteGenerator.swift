//
//  PaletteGenerator.swift
//  Pholor
//
//  Created by Jakub Ziembiński on 01/03/2017.
//  Copyright © 2017 Jakub Ziembiński. All rights reserved.
//

import UIKit

/**
 Holds colors generated from image provided by the User.
    * palette: **Multiple variables / array / set**
 */
public struct Palette {
    var c1: UIColor!
    var c2: UIColor!
    var c3: UIColor!
    var c4: UIColor!
    var c5: UIColor!
}

/**
 Holds number of ocurrances of single UIColor.
    * color: UIColor
    * count: Int
 */
class CountedColor {
    var color: UIColor
    var count: Int
    
    init(color: UIColor, count: Int) {
        self.count = count
        self.color = color
    }
}

extension CGColor {
    var rgba: [CGFloat] {
        get {
            var red = CGFloat()
            var green = CGFloat()
            var blue = CGFloat()
            var alpha = CGFloat()
            UIColor(cgColor: self).getRed(&red, green: &green, blue: &blue, alpha: &alpha)
            return[red, green, blue, alpha]
        }
    }
}

extension UIColor {
    
    /**
     Check if the color is close to being black or white.
     
     - returns: `true` if color is black or white
     */
    var isBlackOrWhite: Bool {
        let rgba = self.cgColor.rgba
        let max: CGFloat = 0.91
        let min: CGFloat = 0.09
        return (rgba[0] > max && rgba[1] > max && rgba[2] > max) || (rgba[0] < min && rgba[1] < min && rgba[2] < min)
    }
    
    /**
     Check if the colors are distinguishable to human eye.
     
     - parameter colorToCompare:
     
     - returns: `true` if colors are different
    */
    func differs(from colorToCompare: UIColor) -> Bool {
        var c1 = self.cgColor.rgba
        var c2 = colorToCompare.cgColor.rgba
        let threshold: CGFloat = 0.20
        
        if fabs( c1[0] - c2[0]) > threshold || fabs(c1[1] - c2[1]) > threshold || fabs(c1[2] - c2[2]) > threshold {
            return true
        }
        
        return false
    }
    
    
}

extension UIImage {
    
    /**
     Resizes image to newSize. That means shrinking but also stretching basic image.
     
     - parameter newSize: size (`CGSize`) to which image will be redrawn.
     
     - returns: resised `UIImage`
    */
    func resizeImageTo(size newSize: CGSize) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1)
        defer {
            UIGraphicsEndImageContext()
        }
        self.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        guard let result = UIGraphicsGetImageFromCurrentImageContext() else {
            fatalError("UIImageColors.resizeForUIImageColors failed: UIGraphicsGetImageFromCurrentImageContext returned nil")
        }
    
        print("resizeImageTo: \nWIDTH:\(result.cgImage!.width)\nHEIGHT:\(result.cgImage!.width)")
        return result
    }
    
    public func generatePalette(completionHandler: @escaping (Palette) -> Void) {
        DispatchQueue.global().async {
            let result = self.generatePalette()
            
            DispatchQueue.main.async {
                completionHandler(result)
            }
        }
    }
    
    public func generatePalette() -> Palette {
        
        let ratio: CGFloat = self.size.width/self.size.height
        let scaleWidth: CGFloat = 250
        let scaleHeight: CGFloat = scaleWidth/ratio
        
        let scaleSize = CGSize(width: scaleWidth, height: scaleHeight)
        
        let cgImage = self.resizeImageTo(size: scaleSize).cgImage!
        let width = cgImage.width
        let height = cgImage.height
        
        let bytesPerPixel: Int = 4
        let bytesPerRow = width * bytesPerPixel
        let bitsPerComponent: Int = 8
        let randomColorThreshold = Int(CGFloat(height)*0.01)
        let colorComparator: Comparator = { (color1, color2) -> ComparisonResult in
            let c1 = color1 as! CountedColor, c2 = color2 as! CountedColor
            if c1.count < c2.count {
                return ComparisonResult.orderedDescending
            } else if c1.count == c2.count {
                return ComparisonResult.orderedSame
            } else { return ComparisonResult.orderedAscending }
        }
        
        let rawData = malloc(bytesPerRow * height)
        defer {
            free(rawData)
        }
        let bitmapInfo = CGImageAlphaInfo.premultipliedFirst.rawValue
        guard let context = CGContext(data: rawData, width: width, height: height, bitsPerComponent: bitsPerComponent, bytesPerRow: bytesPerRow, space: CGColorSpaceCreateDeviceRGB(), bitmapInfo: bitmapInfo) else {
            fatalError("PaletteGenerator.generatePalette failed: could not create CGBitmapContext")
        }
        let drawingRect = CGRect(x: 0, y: 0, width: CGFloat(width), height: CGFloat(height))
        context.draw(cgImage, in: drawingRect)
        
        guard let data = context.data?.assumingMemoryBound(to: UInt8.self) else {
            fatalError("PaletteGenerator.generatePalete failed: could not get data from Context")
        }
        
        let basicColors = NSCountedSet(capacity: width * height)
        
        for i in stride(from: 0, to: width * height * 4, by: 4){
            let pixelColor = UIColor(
                    red: CGFloat(data[i+1])/255,
                    green: CGFloat(data[i+2])/255,
                    blue: CGFloat(data[i+3])/255,
                    alpha: 1
                )
            basicColors.add(pixelColor)
        }
        
        let sortedColors = NSMutableArray(capacity: basicColors.count)
        let enumerator = basicColors.objectEnumerator()
        
        while let color = enumerator.nextObject() as? UIColor {
            let count = basicColors.count(for: color)
            if count > randomColorThreshold {
                sortedColors.add(CountedColor(color: color, count: count))
            }
        }
        
        sortedColors.sort(comparator: colorComparator)
        var result = Palette()
        
        var firstColor = true
        var colorsGot = 0
        
        for i in 0..<sortedColors.count {
            let countedColor = sortedColors[i] as! CountedColor
            let color = countedColor.color
            if firstColor {
                result.c1 = color
                firstColor = false
                colorsGot += 1
            } else {
                switch colorsGot {
                case 1:
                    if(color.differs(from: result.c1) && !color.isBlackOrWhite) {
                        result.c2 = color
                        colorsGot += 1
                    }
                case 2:
                    if(color.differs(from: result.c1) && color.differs(from: result.c2) && !color.isBlackOrWhite) {
                        result.c3 = color
                        colorsGot += 1
                    }
                case 3:
                    if(color.differs(from: result.c1) && color.differs(from: result.c2) && color.differs(from: result.c3) && !color.isBlackOrWhite) {
                        result.c4 = color
                        colorsGot += 1
                    }
                case 4:
                    if(color.differs(from: result.c1) && color.differs(from: result.c2) && color.differs(from: result.c3) && color.differs(from: result.c4) && !color.isBlackOrWhite) {
                        result.c5 = color
                        colorsGot += 1
                    }
                default:
                    break
                }
            }
            
            if colorsGot == 5 {
                break
            }
        }
        return result
    }
}
