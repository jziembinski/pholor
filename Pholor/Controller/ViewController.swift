//
//  ViewController.swift
//  Pholor
//
//  Created by Jakub Ziembiński on 04/01/2017.
//  Copyright © 2017 Jakub Ziembiński. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UINavigationControllerDelegate {

    @IBOutlet weak var imageSelectLabel: UILabel!
    @IBOutlet weak var imageSelectView: UIView!
    
    @IBOutlet weak var cameraButton: UIButton!
    @IBOutlet weak var galleryButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) == false {
            cameraButton.isEnabled = false
        }
        
    }

    @IBAction func addPhotoFromGallery(_ sender: Any) {
        let picker = UIImagePickerController()
        picker.allowsEditing = true
        picker.delegate = self
        picker.sourceType = .photoLibrary
        present(picker, animated: true)
    }
    
    @IBAction func addPhotoFromCamera(_ sender: Any) {
        let picker = UIImagePickerController()
        picker.sourceType = .camera
        picker.delegate = self         
        present(picker, animated: true)
    }
    
    
}

extension ViewController: UIImagePickerControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let pickedImage: UIImage
        
        if let editedImage = info["UIImagePickerControllerEditedImage"] as? UIImage {
            pickedImage = editedImage
        } else if let originalImage = info["UIImagePickerControllerOriginalImage"] as? UIImage {
            pickedImage = originalImage
        } else {
            return
        }
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "ExtractedPaletteViewController") as! ExtractedPaletteViewController
        vc.pickedImage = pickedImage
        dismiss(animated: true)
        navigationController?.pushViewController(vc, animated: true)
    }
    
}

