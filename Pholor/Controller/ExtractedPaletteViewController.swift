//
//  ExtractedPaletteViewController.swift
//  Pholor
//
//  Created by Stanisław Paśkowski on 05.01.2017.
//  Copyright © 2017 Jakub Ziembiński. All rights reserved.
//

import UIKit

class ExtractedPaletteViewController: UIViewController {

    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet public var colorButtons: [UIButton]!
    @IBOutlet weak var selectInfoView: UIView!
    
    var pickedImage: UIImage!
    var swipeRecognizer: UISwipeGestureRecognizer!
    var activeColorButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configSwipeUp()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        photoImageView.image = pickedImage
        setColorButtons()
    }
    
    func setColorButtons() {
        if let image = self.photoImageView.image {
            image.generatePalette { palette in
                self.colorButtons[0].backgroundColor = palette.c1
                self.colorButtons[1].backgroundColor = palette.c2
                self.colorButtons[2].backgroundColor = palette.c3
                self.colorButtons[3].backgroundColor = palette.c4
                self.colorButtons[4].backgroundColor = palette.c5
            }
        }
    }
    
    @IBAction func colorButtonTapped(_ sender: Any) {
        guard let tappedButton = sender as? UIButton else { return }
        activeColorButton = tappedButton
        activeColorButton.layer.zPosition += 1
        
        UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseIn], animations: {
            self.view.frame.origin.y -= 375
        }, completion: nil)
        
        UIView.animate(withDuration: 0.2, delay: 0.251, options: [.curveEaseIn], animations: {
            tappedButton.frame.origin.y += 74
            tappedButton.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
            
        }, completion: nil)
        
        for button in colorButtons {
            button.isEnabled = false
        }
        
        selectInfoView.isHidden = true
        swipeRecognizer.isEnabled = true
    }
    
    func configSwipeUp() {
        swipeRecognizer = UISwipeGestureRecognizer()
        swipeRecognizer.direction = .down
        swipeRecognizer.numberOfTouchesRequired = 1
        swipeRecognizer.addTarget(self, action: #selector(swipeUpSwiped))
        swipeRecognizer.isEnabled = false
        
        
        
        view.addGestureRecognizer(swipeRecognizer)
    }
    
    func swipeUpSwiped() {
        
        UIView.animate(withDuration: 0.1, delay: 0.0, options: [.curveEaseOut], animations: {
            self.activeColorButton.frame.origin.y -= 74
            self.activeColorButton.transform = CGAffineTransform.identity
        })
        
        UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseIn], animations: {
            self.view.frame.origin.y += 375
        }, completion: {
            (finished: Bool) -> Void in
            self.activeColorButton.layer.zPosition -= 1
        })
        
        for button in colorButtons {
            button.isEnabled = true
        }
        
        swipeRecognizer.isEnabled = false
    }
    
}
