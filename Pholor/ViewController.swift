//
//  ViewController.swift
//  Pholor
//
//  Created by Jakub Ziembiński on 04/01/2017.
//  Copyright © 2017 Jakub Ziembiński. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UINavigationControllerDelegate {

    @IBOutlet weak var imageSelectLabel: UILabel!
    @IBOutlet weak var imageSelectView: UIView!
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet public var colorLabels: [UILabel]!
    @IBOutlet weak var colorsStackView: UIStackView!
    
    @IBOutlet weak var cameraButton: UIButton!
    @IBOutlet weak var galleryButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageView.isHidden = true
        colorsStackView.isHidden = true
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) == false {
            cameraButton.isEnabled = false
        }
        
    }

    @IBAction func addPhotoFromGallery(_ sender: Any) {
        let picker = UIImagePickerController()
        picker.allowsEditing = true
        picker.delegate = self
        picker.sourceType = .photoLibrary
        present(picker, animated: true)
    }
    
    @IBAction func addPhotoFromCamera(_ sender: Any) {
        let picker = UIImagePickerController()
        picker.sourceType = .camera
        picker.delegate = self         // DODAŁEM delegate BO NIE BYŁO
        present(picker, animated: true)
    }
    
    func setColorLabels() {
        if let image = self.imageView.image {
            image.getColors { colors in
                self.colorLabels[0].backgroundColor = colors.backgroundColor
                self.colorLabels[1].backgroundColor = colors.detailColor
                self.colorLabels[2].backgroundColor = colors.primaryColor
                self.colorLabels[3].backgroundColor = colors.secondaryColor
            }
        }
    }
    
    func hidePhotoSelect() {
        imageSelectView.isHidden = true
        imageSelectLabel.isHidden = true
        
        self.imageView.isHidden = false
        self.colorsStackView.isHidden = false
    }
    
}

extension ViewController: UIImagePickerControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let editedPhoto = info["UIImagePickerControllerEditedImage"] as? UIImage {
            self.imageView.image = editedPhoto
        } else if let originalPhoto = info["UIImagePickerControllerOriginalImage"] as? UIImage {
            self.imageView.image = originalPhoto
        } else {
            return
        }
        
        self.setColorLabels()
        self.hidePhotoSelect()
        
        dismiss(animated: true)
    }
    
}

